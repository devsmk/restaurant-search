import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import SearchBar from '../components/SearchBar';
import useBusinesses from '../hooks/useBusinesses';
import BusinessesList from '../components/BusinessesList';

const SearchScreen = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchApi, businesses, errorMessage] = useBusinesses();

  // price = '$' || '$$' || '$$$'
  const filterBusinessesByPrice = (price) => {
    return businesses.filter((result) => {
      return result.price === price;
    });
  };

  return (
    <>
      <SearchBar
        searchTerm={searchTerm}
        onSearchTermChange={setSearchTerm}
        onSearchTermSubmit={() => searchApi()}
      />
      {errorMessage !== null ? (
        <Text style={styles.error}>{errorMessage}</Text>
      ) : null}
      <ScrollView>
        <BusinessesList
          businesses={filterBusinessesByPrice('$')}
          priceCategory="Cost Effective"
          price="$"
        />
        <BusinessesList
          businesses={filterBusinessesByPrice('$$')}
          priceCategory="Good Eats"
          price="$$"
        />
        <BusinessesList
          businesses={filterBusinessesByPrice('$$$')}
          priceCategory="Big Spender"
          price="$$$"
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  error: {
    color: 'red',
    fontSize: 16,
    marginTop: 5,
    textAlign: 'center',
  },
});

export default SearchScreen;
