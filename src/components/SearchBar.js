import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TextInput } from 'react-native-gesture-handler';

const SearchBar = ({ searchTerm, onSearchTermChange, onSearchTermSubmit }) => {
  return (
    <View style={styles.backgroundStyle}>
      <Ionicons name="ios-search" style={styles.iconStyle} />
      <TextInput
        style={styles.inputStyle}
        value={searchTerm}
        onChangeText={onSearchTermChange}
        placeholder="Search"
        autoCapitalize="none"
        onEndEditing={onSearchTermSubmit}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundStyle: {
    backgroundColor: '#D4D4D4',
    height: 50,
    borderRadius: 6,
    marginVertical: 15,
    marginHorizontal: 15,
    flexDirection: 'row',
  },
  inputStyle: {
    flex: 1,
    fontSize: 18,
  },
  iconStyle: {
    fontSize: 22,
    alignSelf: 'center',
    marginHorizontal: 8,
  },
});

export default SearchBar;
