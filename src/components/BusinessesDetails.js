import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const BusinessesDetails = ({ business }) => {
  return (
    <View style={styles.container}>
      <Image source={{ uri: business.image_url }} style={styles.image} />
      <Text style={styles.name}>{business.name}</Text>
      <Text>
        {business.rating} Stars, {business.review_count} Reviews
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 15,
  },
  image: {
    width: 260,
    height: 160,
    borderRadius: 4,
    marginBottom: 5,
  },
  name: {
    fontWeight: 'bold',
  },
});

export default BusinessesDetails;
