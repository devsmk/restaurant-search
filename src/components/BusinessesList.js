import React, { Children } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import BusinessesDetails from './BusinessesDetails';

const BusinessesList = ({ priceCategory, businesses, price }) => {
  return (
    <View style={styles.container}>
      <View style={styles.priceCategoryContainer}>
        <Text style={styles.priceCategoryStyle}>{priceCategory}</Text>
        <Text style={styles.priceMarker}>{price}</Text>
      </View>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={businesses}
        keyExtractor={(business) => business.id}
        renderItem={({ item }) => {
          return <BusinessesDetails business={item} />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  priceCategoryStyle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  priceCategoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 15,
    marginBottom: 5,
  },
  priceMarker: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 10,
  },
});

export default BusinessesList;
