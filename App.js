import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SearchScreen from './src/screens/SearchScreen';
import BusinessesDrillDownScreen from './src/screens/BusinessesDrillDownScreen';

const navigator = createStackNavigator(
  {
    Search: SearchScreen,
    DrillDown: BusinessesDrillDownScreen,
  },
  {
    initialRouteName: 'Search',
    defaultNavigationOptions: {
      title: 'Restaurant Search',
    },
  }
);

export default createAppContainer(navigator);
